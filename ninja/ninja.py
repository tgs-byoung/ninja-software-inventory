import requests


class Application:
    base_url: str
    client_id: str
    client_secret: str
    scope: list[str]
    grant_type: str
    access_token: str
    page_size: int

    def __init__(
        self,
        base_url: str,
        client_id: str,
        client_secret: str,
        scope: list[str],
        grant_type: str,
        page_size: int,
    ):
        self.base_url = base_url
        self.client_id = client_id
        self.client_secret = client_secret
        self.scope = scope
        self.grant_type = grant_type
        self.page_size = page_size
        self.access_token = self.get_access_token()

    def format_header(self) -> dict:
        """Format the header for the Ninja API with the required authorization."""
        return {"Authorization": f"Bearer {self.access_token}"}

    def format_auth_body(self) -> str:
        """Format the auth body for the Ninja API."""
        scopes = " ".join(self.scope)
        return (
            f"client_id={self.client_id}&client_secret={self.client_secret}"
            f"&scope={scopes}&grant_type={self.grant_type}"
        )

    def get_access_token(self) -> str:
        """Retreive access token from the Ninja API."""
        url = f"{self.base_url}/ws/oauth/token"
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        data = self.format_auth_body()
        response = requests.post(url, headers=headers, data=data)
        return response.json().get("access_token")

    def get_all_devices(self, device_filter: str = None) -> list[dict]:
        """Retreieve all devices from the Ninja API."""
        devices = []
        latest_device_id = 0
        page = 1
        while len(devices) == (page - 1) * self.page_size:
            devices += self.get_device_page(latest_device_id, device_filter)
            latest_device_id = max([device.get("id") for device in devices])
            page += 1
        return devices

    def get_device_page(
        self, after_device_id: int, device_filter: str = None
    ) -> list[dict]:
        """Retreive a page of devices from the Ninja API."""
        url = f"{self.base_url}/api/v2/devices"
        headers = self.format_header()
        params = {
            "after": after_device_id,
            "pageSize": self.page_size,
        }

        # If the user provides a device filter, we'll include it in our parameters
        if device_filter:
            params["df"] = device_filter

        response = requests.get(url, headers=headers, params=params)

        if response.status_code != 200:
            raise Exception("Unable to retrieve devices from Ninja API.")

        return response.json()

    def get_all_organizations(self) -> list[dict]:
        """Retrieves a list of all organizations from the Ninja API."""
        organizations = []
        latest_organization_id = 0
        page = 1
        while len(organizations) == (page - 1) * self.page_size:
            organizations += self.get_organization_page(latest_organization_id)
            organization_ids = [
                organization.get("id") for organization in organizations
            ]
            latest_organization_id = max(organization_ids)
            page += 1
        return organizations

    def get_organization_page(self, after_organization_id: int) -> list[dict]:
        """Retrieves a page of organizations from the Ninja API."""
        url = f"{self.base_url}/api/v2/organizations"
        headers = self.format_header()
        params = {
            "after": after_organization_id,
            "pageSize": self.page_size,
        }
        response = requests.get(url, headers=headers, params=params)
        return response.json()

    def get_device_software(self, device_id: int) -> list[dict]:
        """Retrieves a list of software installed on a device from the Ninja API."""
        url = f"{self.base_url}/api/v2/device/{device_id}/software"
        headers = self.format_header()
        response = requests.get(url, headers=headers)

        if response.status_code != 200:
            raise Exception("Unable to retrieve software from Ninja API.")

        return response.json()
