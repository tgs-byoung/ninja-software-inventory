import config
import logging
import argparse
from ninja import Application

CONFIG_FILE_PATH = "./config/config.yaml"
NODE_TYPES_TO_SCAN = ["WINDOWS_WORKSTATION", "WINDOWS_SERVER", "MAC"]


logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(module)s.%(funcName)s %(message)s",
    datefmt="%Y-%m-%dT%H:%M:%S %z",
)


def initialize_csv(csv_file_path: str) -> None:
    """Initialize the CSV file."""
    with open(csv_file_path, "w") as csv_file:
        csv_file.write(
            '"DeviceName","DeviceType","LastContact","LastUpdate","Name","Version","Publisher","Location","InstallDate","ProductCode"\n'
        )


def export_to_csv(device: dict, softwares: list[dict], csv_file_path: str) -> None:
    """Export all software to a CSV file."""
    with open(csv_file_path, "a") as file:
        for software in softwares:
            line = (
                f'"{device.get("systemName")}",'
                f'"{device.get("nodeClass")}",'
                f'"{device.get("lastContact")}",'
                f'"{device.get("lastUpdate")}",'
                f'"{software.get("name")}",'
                f'"{software.get("version")}",'
                f'"{software.get("publisher")}",'
                f'"{software.get("location")}",'
                f'"{software.get("installDate")}",'
                f'"{software.get("productCode")}"\n'
            )
            file.write(line)


def export_software_for_device(
    device: dict, ninja_app: Application, csv_file_path: str
) -> None:
    """Scan and export all software for a device to a csv."""
    softwares = []
    logging.info(
        f"Retrieving and exporting software for device {device.get('systemName')}."
    )
    softwares += ninja_app.get_device_software(device.get("id"))
    export_to_csv(device, softwares, csv_file_path)
    logging.info(
        f"Successfully retreieved and exported software for device {device.get('systemName')}."
    )


def main() -> None:
    conf = config.Config(CONFIG_FILE_PATH)

    # Parsing command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-o",
        "--organization",
        help="Organization name to scan.",
        type=str,
        required=True,
    )
    parser.add_argument(
        "-f",
        "--file",
        help="Path to the CSV file to export the software to.",
        type=str,
        default="softwares.csv",
    )
    parser.add_argument(
        "-a",
        "--append",
        help="Append the software to the CSV file.",
        type=bool,
        default=False,
    )
    args = parser.parse_args()

    # Initialize the Ninja API
    ninja = Application(
        conf.base_url,
        conf.client_id,
        conf.client_secret,
        conf.scope,
        conf.grant_type,
        conf.page_size,
    )

    # Retrieve all organizations that are in Ninja
    logging.info("Retrieving all organizations.")
    organizations = ninja.get_all_organizations()
    logging.info("Successfully retreieved all organizations.")

    # Filter for the organization that is in scope
    logging.info(
        f"Found {len(organizations)} organizations, filtering for the organization {args.organization}.)"
    )
    organizations_to_scan = []
    for organization in organizations:
        if organization.get("name") == args.organization:
            organizations_to_scan.append(organization)
    if len(organizations_to_scan) == 0:
        logging.error(f"Organization {args.organization} not found.")
        exit(1)
    elif len(organizations_to_scan) > 1:
        logging.error(
            f"Found more than one organization with the name {args.organization}."
        )
        exit(1)
    logging.info(f"Successfully retreieved organization {args.organization}.")

    # Retrieve all devices for each organization that is in scope
    logging.info(
        f"Retrieving all devices for {len(organizations_to_scan)} organizations."
    )
    devices_to_scan = []
    for organization in organizations_to_scan:
        devices_to_scan += ninja.get_all_devices(f"org={organization.get('id')}")
    logging.info(f"Found {len(devices_to_scan)} devices to scan.")

    # Initializing the CSV file if we aren't appending to an existing file
    if not args.append:
        initialize_csv(args.file)

    # Retrieve all device software inventory for each device that is in scope
    logging.info(f"Retrieving all software for {len(devices_to_scan)} devices.")
    softwares = []
    for device in devices_to_scan:
        if device.get("nodeClass") not in NODE_TYPES_TO_SCAN:
            logging.warning(
                f"Skipping device {device.get('systemName')} - {device.get('nodeClass')}."
            )
            continue
        export_software_for_device(device, ninja, args.file)

    # Export all software to a CSV file
    logging.info(f"Exporting all software to `{args.file}`.")
    # export_to_csv(softwares)
    logging.info(f"Successfully exported all software to `{args.file}`.")


if __name__ == "__main__":
    main()
