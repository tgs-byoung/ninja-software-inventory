import yaml


class Config:
    base_url: str
    client_secret: str
    client_id: str
    organizations: list[str]
    scope: list[str]
    grant_type: str
    page_size: int

    def __init__(self, config_file: str):
        with open(config_file) as file:
            raw_config = yaml.safe_load(file)
            self.base_url = raw_config.get("ninja").get("base_url")
            self.client_secret = raw_config.get("ninja").get("client_secret")
            self.client_id = raw_config.get("ninja").get("client_id")
            self.organizations = raw_config.get("ninja").get("organizations")
            self.scope = raw_config.get("ninja").get("scope")
            self.grant_type = raw_config.get("ninja").get("grant_type")
            self.page_size = raw_config.get("ninja").get("page_size")
