# NinjaOne Software Inventory

## Background

This project was originally spawned by [Service Ticket #433919 - Non Standard Software report](https://na.myconnectwise.net/v4_6_release/services/system_io/Service/fv_sr100_request.rails?service_recid=433919&companyName=tgs) which is a request from Don Newsom in Q1 of 2023 to retrieve a list of "non-standard" software that is installed on users machines.

This was originally attempted inside Ninja itself using the built-in reporting functionality, but it proved to not be able to export in a nice manner (csv/excel) for further manipulation of the data. The only export option was a PDF which made it hard to get the data into another application (excel) for analysis.

With the help of Amanda I attempted to do this in Brightgauge. We were able to find the required information, however, we found that there was a limitation of 500 entries that it would display.

This meant that there wasn't really a better option than to extract the data from the Ninja API's.

## Setup

Copy the existing configuration file at `./config/config.example.yaml` to `./config/config.yaml` and fill in all required fields.

### Get NinjaOne API Key

1. Navigate to https://tgs.rmmservice.com

2. Navigate to Administrator > Apps > API > Client App IDs

3. Click **Add** to add a new API Key

4. Select **API Services (machine-to-machine)** from the dropdown

5. Enter a name (please include the date)

6. Leave **Redirect URIs** blank

7. Select **monitoring** for the scope

8. Select only **Client Credentials** for the _Allowed Grant Types_

9. Click **SAVE**

10. Copy the client secret and set it as **ninja > client_secret** in [./config/config.yaml](./config/config.yaml)

11. Close the client secret pop-up

12. Copy the **Client ID** and set it as the **ninja > cleint_id** in [./config/config.yaml](./config/config.yaml)

> **NOTE: Please ensure that you remove the Client/API key from Ninja once you are done**

## Running

To run the program, do the following:

> Your python install might be `python`. This is more likely if you are on Windows.

1. Create a Virtual Environment using Python
   `python3 -m venv .venv`

2. Activate the virtual environment
   `source .venv/bin/activate`

3. Install the required packages using pip
   `pip install -r requirements.txt`

4. Run the program specifying the following parameters:
   `python ./main.py -o "Organization Name" -f "output.csv"`

The program may take a few minutes to run and will provide information about where it is in the process.

## Debugging

If you run into any instance where this program doesn't run correctly or pulls inaccurate data please reach out to the Maintainer of this project.
